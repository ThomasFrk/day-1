Day 1
What I did today? Installed a new version of nodeJS, did a little bit of an Angular course, learned about gitlab and it's integration with webstorm,
learned about ssh keys
What I expect to do tomorrow? Basic exercises with arrays and strings.
What were my blockers? Switching from github to gitlab, and checking if everything is working

Day 2
What I did today? Refreshed my knowledge on array and object basics, and string and array manipulation, learned more about git, and udemy angular course
What I expect to do tomorrow? More exercises and the udemy course
What were my blockers? The object basics exercise, trying to remember basic knowledge
